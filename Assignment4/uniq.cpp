#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
#include <vector>
#include <set>
#include <map>
using std::set;
using std::map;
using std::vector;
using std::cin;
using std::cout;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of uniq.  Supported options:\n\n"
"   -c,--count         prefix lines by their counts.\n"
"   -d,--repeated      only print duplicate lines.\n"
"   -u,--unique        only print lines that are unique.\n"
"   --help             show this message and exit.\n";

int main(int argc, char *argv[]) {
	// define long options
	static int showcount=0, dupsonly=0, uniqonly=0;
	static struct option long_opts[] = {
		{"count",         no_argument, 0, 'c'},
		{"repeated",      no_argument, 0, 'd'},
		{"unique",        no_argument, 0, 'u'},
		{"help",          no_argument, 0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cduh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				showcount = 1;
				break;
			case 'd':
				dupsonly = 1;
				break;
			case 'u':
				uniqonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	/*
	string word = "Hello my \n name is \n test";
	vector<string> tempVector;

	//if we encounter a space then we get the count
	//get line count
	
	//Split the string up using npos and find functions on the string. 	
	size_t wordPosition;
	while ( (wordPosition = word.find('\n') != std::string::npos) ) { //if we find a newline and we aren't at the max value of size_t
		tempVector.push_back( word.substr(0, wordPosition) ); //will store the first line.
		word = word.substr(wordPosition + 1); //will set word to one space after the newline to keep getting characters.
	}
	tempVector.push_back(word); //Now tempVector will contain the string line by line conveniently in diff. elements
	
	for (size_t j = 0; j < tempVector.size(); j++) {
		cout << tempVector[j] << "\n";
	} */
	
	// -c show the count before every line
	
	string word;
	getline(cin, word);
	
    vector<string> vectorofWords;
    set<string> duplicatedWords;
    set<string> uniqueWords;
    /*
    for (int i = 0; i < word.length(); i++) {
       //    int initialPosition = 0;
       //    int iteratedPosition = i;

        if (isspace(word[i])) { //If this index is a space
             //set k equal to one space after to keep incrementing until the next space.
            //Getting the index of the next space

           // cout << i << endl;
            for ( int j = i-k; j < i ; j++) {
                newString += word[j];
            }
            k = i;
            cout << newString << endl;
            newString.clear();
        }
    } */ //^^Above only prints inside words of stdin.
 
    size_t wordPosition;
    //Sort of like going through a set we use find as an interator until we detect a \n in the string.
    //npos refers to the max value of size_t
    while( (wordPosition = word.find('\n')) != std::string::npos ) {
    	
    	//Will push back a substring of the beginning until \n
        vectorofWords.push_back( word.substr(0,wordPosition) );
        
        //makes word into a new string of everything after that \n we just found.
        //Wordposition is located at the next \n index.
        word = word.substr(wordPosition+1);
        
    }
    vectorofWords.push_back(word);
    
    int counter = 1;
    
    for(wordPosition = 0; wordPosition < vectorofWords.size(); ++wordPosition) {
    	
        cout << counter << " " << vectorofWords[wordPosition] << "\n";
        
        ++counter;
        
    }
	
	// END OF C BLOCK
	
	// D BLOCK - Duplicated Lines
	
	int dupeCounter;
	
	for (size_t i = 0; i < vectorofWords.size(); i++) {
		
		dupeCounter = 0;
		
		for (size_t j = 0; j < vectorofWords.size(); j++) {
			
			if (vectorofWords[i] == vectorofWords[j]) {
				dupeCounter++;
			}
			
		}
		
		if (dupeCounter > 1) {
			duplicatedWords.insert(vectorofWords[i]);
		}
		
	}
	
	cout << "Duplicated Lines:" << std::endl;
	for (set<string>::iterator iter = duplicatedWords.begin(); iter != duplicatedWords.end(); iter++) {
		cout << *iter << "\n";
	}
	
	// END OF D BLOCK
	
	//U BLOCK (Unique lines)
	int uniqueCounter;
	
	for (size_t i = 0; i < vectorofWords.size(); i++) {
		
		uniqueCounter = 0;
		
		for (size_t j = 0; j < vectorofWords.size(); j++) {
			
			if (vectorofWords[i] == vectorofWords[j]) {
				uniqueCounter++;
			}
			
		}
		
		if (uniqueCounter == 1) {
			uniqueWords.insert(vectorofWords[i]);
		}
		
	}
	
	cout << "Unique Lines:" << std::endl;
	for (set<string>::iterator iter = uniqueWords.begin(); iter != uniqueWords.end(); iter++) {
		cout << *iter << "\n";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	return 0;
}
