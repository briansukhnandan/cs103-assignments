
#include <iostream>
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <set>
using std::set;
using std::endl;
#include <getopt.h> // to parse long arguments.
#include <cstdio> // printf
using namespace std;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";

int main(int argc, char *argv[])
{
	
	string word;
	int byteSize = sizeof(word);
	size_t wordPosition;
	vector<string> vectorofWords;
	set<string> stringUnique;
	int lineCount = 1;
	int wordCount = 1;
		
	// define long options
	static int charonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				charonly = 1;
				cout << byteSize << endl;
				break;
			case 'l':
				linesonly = 1;
				cout << lineCount << endl;
				break;
			case 'w':
				wordsonly = 1;
				cout << wordCount << endl;
				break;
			case 'u':
				uwordsonly = 1;
				for (set<string>::iterator k = stringUnique.begin(); k!= stringUnique.end(); k++) {
    				cout << *k << '\n';
				} 
				break;
			case 'L':
				longonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
    
    string word;
    getline(cin, word);
    
    unsigned int wordLength = word.length();
    //iterate through the string, get the indexes of the spaces, indicates words
    int k;

    set<string> setofArray;
    string newString;
    
//LINE COUNT BLOCK
	for (unsigned int j = 0; j < word.length(); j++) {
		if (word[j] == '\n') {
			lineCount++;
		}
	}
// **END** LINE COUNT BLOCK
    
//WORD COUNT BLOCK
    for (unsigned int i = 0; i < word.length(); i++) {
    	if (isspace(word[i]) ) { //If the string contains a whitespace, then up the wordcount by 1.
    		wordCount++;
		}
	}
// **END** WORD COUNT BLOCK

// BYTE SIZE BLOCK
// **END** BYTE SIZE BLOCK

	cout << lineCount << "	" << wordCount << "	" << byteSize << endl;
	cout << "The unique words are: " << endl;
	//UNIQUE WORD BLOCK
	//Puts each word into a vector
	//npos is the max value of size_t. it will search the entire string until the max_value of size_t
	//is reached.
	while( (wordPosition = word.find("\n")) != std::string::npos ) {
    	
    	//Will push back a substring of the beginning until \n
        vectorofWords.push_back( word.substr(0,wordPosition) );
        
        //makes word into a new string of everything after that \n we just found.
        //Wordposition is located at the next \n index.
        word = word.substr(wordPosition+1);
        
    }
    //Finally push each word of the sentence into a vector.
    vectorofWords.push_back(word);
    
    //Push each of the vector into a set to make it unique.
    for (int j = 0; j < vectorofWords.size(); j++) {
    	
    	stringUnique.insert(vectorofWords[j] );
    	
	}
	
	for (set<string>::iterator k = stringUnique.begin(); k!= stringUnique.end(); k++) {
    	cout << *k << '\n';
	} 
    //END UNIQUE WORD BLOCK
	/*
	for (set<string>::iterator iter = setofArray.begin(); iter != setofArray.end(); iter++) {
		cout << *iter << endl;
	} */

	return 0;
}
