#include <iostream>
//using std::string;
//using std::cin;
//using std::cout;
//using std::endl; //or you can use using std::cout; and using std::cin;
using namespace std;
#include <cmath>
#include <math.h>
#include <vector>
#include <algorithm>

// 2/1/18

//note on the virtual machine you can open files by "open filename.cpp"
//variables: box to hold a value
//datatype: "shape" of the box
//pretend x is a box that holds an integer.
//sizeof() operator tells the size byte wise of something.
//g++ filename.cpp && ./a.out to compile
//for float datatypes you can also use double.
/*NOTE: Because double does not have arbitrary precision, some algebraic laws will not work! 
Ex: 0.00000000000001 and 0.0 will be the same value if both labelled as doubles*/
//returning 0 means the program executed normally without any issues.

/*
void hi(int x, int y, int z) {
    cout << (x + y + z) / 3 << endl;
}

int main() {
    cout << "Hello World!" << endl;
    
    hi(20, 30, 40);
    
    
    return 0;
}
*/
/*
int main() {
    
    int d = 0;
    
    double a = 0.0;
    double E = 0.00000000001;
    
    if (d+E == d+a) {
        cout << "e is equal to a" << endl;
    }
    //will execute the if statement.
    return 0;
}
*/
/*
int main() {
    
    int lowest_number = rand() % 50 + 1;
    int highest_number = rand() % (100 - 50) + 50; // (high - low) + low
    int number_guessed;
    
    cout << lowest_number << endl;
    cout << highest_number << endl;
    
    cin >> number_guessed;
    
    cout << "Please enter a number. \n";
    
    if (number_guessed > lowest_number && number_guessed < highest_number) {
            highest_number = number_guessed;
            cout << highest_number << "\n";
        }
        else {
            cout << "Try again";
        }
    
    return 0;
}

/*
int main () {
    
    int x = rand() % 10 + 1;
    int y = rand() % (20 - 10) + 10;
    
    cout << x << endl;
    cout << y << endl;
    
    return 0;
}
*/
/*
int main () {
    
    float answer = 0.0;
    int x; int y; int z;
    cout << "Please enter 3 numbers.\n";
    cin >> x;
    cin >> y;
    cin >> z;
    
    cout << "Your total before getting the average is: " << (x + y + z) << endl;
        
    cout << ((x + y + z) / 3) << "\n";
    
    return 0;
}
*/
/*
int main() {
    
    int size_of_array = 5;
    int array_of_numbers[size_of_array];
    int sum = 0;
    
    //make a loop that for 5 numbers it will get user input and take those numbers and make them a sum.
    for (int i=0; i<size_of_array; i++) {
        
        cout << "Please enter five numbers that you wish to get the average of:" << endl;
        cin >> array_of_numbers[i]; //inputting the array, and the bracket for what number will go inside of it, in this case "i" or the user input.
        
        sum += array_of_numbers[i];
        
    }
    cout << (sum)/(size_of_array) << endl;
    
    return 0;
}
*/
//TODO 2
/*
int main() {
    
    int x;
    int sum = 0;
    
    for (int i=0; i<5; i++) {
        cin >> x;
        sum += x;
        
    }
    
    cout << "Your average is: " << (sum)/5 << endl;
    return 0;
}
*/
//2/6/18
/*cout << 6/7 << endl; this will print the integer, but not the remainder.
to get around this, you can divide an int by a float to get the entire number.
The computer tends to favor floating points.
you can also do cout << x / (double)y << endl; to divide by a double.

if statement general format:
if (any boolean statements can go here) { //be careful when using 1 or 2 equal signs. testing for equality is 2 equal signs.
    //some code here
}else if (some other expression) {
    //some code here
}else {
    //do whatever is in here if both conditions for the if and the else if fail.
    
}

at most, one block of statements will be executed. Precisely one if an else statement is concluded.

LOOPS:
Having something run indefinitely as long as a condition is true.
While loop:
while (true) {
    cout << "hi" << endl; //hitting control+c stops a program when it starts to loop forever
}



*/
/*
int main() {
    
int sum = 0;

    for (int i=0; i < 100; i++) {
        sum += i;
        
    }
    cout << sum << endl;
    */
    /*
    int i=1;
    int sum = 0; // (n * (n+1))/2 will give you the sum of all integers before n.
    
    while (i <= 100) {
        sum += i;
        i++;
    }
    cout << sum << endl;
    
    //for loops are more commonly used when you know the bounds.
    
    return 0;
}
*/
/*
#include <iostream>
using std::string;
using std::cin;
using std::cout;

int main() {
    
    string user_name;
    string relative;
    
    cout << "Enter your name:\n";
    cin >> user_name;
    cout << "Enter a relative (e.g. father, mother):\n";
    cin >> relative;
    
    for (int i=0; i < relative.length(); i++) {
        
        relative[i] = tolower(relative[i]);
    }
    
    cout << "Hello. My name is " << user_name << ". You killed my "<< relative << ". Prepare to die!\n";
    
    return 0;
}
*/
/*
int main() {
    int x = 10, y = 3;
    double d = 4, e = 2;
    //cout << --x << endl;
	
    //cout << x-- << endl;
    //cout << y/x << endl;
    y = e;
    //cout << y/x << endl;
    //cout << x%y << endl;
    cout << (x-d)/y << endl;
	
    return 0;
}
*/
/*
int main()
{
	int x = 16;
	while (x > 0) {
		cout << "x == " << x << "\n";
		x /= 2;
	}
	cout << "x == " << x << "\n";
	return 0;
}
*/
/*
int main()
{
	int x = 9;
	while (x > 0) {
		if (x > 6 || x % 5)
			cout << "x == " << x << "\n";
		x -= 2;
	}
	cout << "x == " << x << "\n";
	return 0;
}
*/
/*
int main()
{
	int i;
	for (i=0; i<15; i++) {
		if (i % 2 == 0 && i % 3 == 0)
			cout << "i == " << i << "\n";
	}
	cout << "i == " << i << "\n";
	return 0;
}
*/
/*
int main() {

    int n;
    int number;
    int min_bound;
    int max_bound;
    cout << "Enter a number that you want to check for divisibility. \n";
    cin >> number;
    cout << "Now enter the exponent bounds you want to check, starting with the lower bound\n";
    cin >> min_bound;
    cin >> max_bound;
    
    if (max_bound <= min_bound) {
        cout << "The max bound is supposed to be greater than the lower bound";
    }else {
        break;
    }
    for (int i=min_bound; i <= max_bound; i++) {
//find the largest exponent of 2 when 2^something is divisible by n
        if ( (int)(pow(2, i)) % (int)number == 0) {
            cout << number << " is divisible by " << pow(2,i) << " which is 2 to the power of " << i << "\n";
        }
    }
    return 0;
}
*/
/*2/8/18
x%5 as a boolean expression returns True if it is greater than 0, basically if the quotient has a remainder.

"Bruteforce" for GCD (greatest common divisor)

gcd (a, b) max number such that (d/a % number = 0 && d/b % number = 0) There exists such a number (q) such that d*q = a.
*/
//start with largest candidate (min (a, b)) and count backwards until the first common divisor is found.
/*
int main() {
    
    int a;
    int b;
    int min;
    int d;
    cin >> a >> b;
    
    if (a < b) {
        min = a;
    }else {
        min = b;
    }
    d = min;
    //while we havent found gcd, keep looking (d--)
    while (a%d != 0 || b%d != 0){
        d--;
    }
    cout << d << "\n";
    
    return 0;
}*/
//Idea: Start with 2^0 and keep multiplying by 2 until division has a remainder.
//Alternative: count the number of times 2 divides the input.
//you can take the number inputted and keep dividing by 2 to find the exponent.
/*
int main () {
    
    int starting_number = pow(2, 0);
    int number;
    cin >> number;
    int k = 0;
    
    while (number%2 == 0) {
        number /= 2;
        k++;
    }
    cout << "2 to the power of " << "  << ;
    
    
    return 0;
}
*/
/*
int main() {
    
    int k; //base
    int n; //number you want to check if n = k^3
    cin >> k >> n;
    
    if (n == pow(k, 3)) {
        
        cout << n << " is " << k << " to the third power! n = k ^ 3";
    }
    
    return 0;
}
*/
/*
int main() {
    
    //we need to loop n times and print the sum of 1^3 + 2^3 ... n^3
    int n;
    int sum = 0;
    cin >> n;
    for (int i=1; i<=n;) {
    
        sum += pow(i, 3);
        i += 2;
    }
    cout << "The sum of all perfect odd cubes up to " << n << "^3 is: " << sum << "\n";
    return 0;
}
*/
/*
int main () {
    
    int number;
    int fibonacci_sum = 0;
    
    cin >> number;
    
    for (int i=1; i<=number; i++) {
        fibonacci_sum += i;
    }
    cout << fibonacci_sum << endl;    
    
    return 0;
}
*/
/*
int main() {
    
    int size_of_array = 6;
    int number[size_of_array];
    int i;
    int result;
    int second_result;
    int lowest_number = number[0];
    int second_lowest_number = number[0];
    
    for (i = 1; i<=(size_of_array); i++) {
        //the user inputs 6 numbers
        cin >> number[i];
        //Finding minimum: If the number inputted is smaller than the lowest_number, the lowest_number becomes that value
        if (number[i] < lowest_number) {
            lowest_number = number[i];
        }
        lowest_number = second_lowest_number;
        if (second_lowest_number > number[i]) {
            second_lowest_number = number[i];
        } //the way to think about it, if the minimum is greater than another number, set that number to the minimum.
    }
    
   
    cout << lowest_number << second_lowest_number << "\n";
    
    for (i = 1; i<=(size_of_array); i++) {
        
        cout << number[i] << " ";
        
    }
    
    return 0;
} */
/*
#include<iostream>
 
using namespace std;
 
// A function to calculate second.
int SecondSmallest(int *a, int n)
{
	int s, ss, i;
	// A variable 's' keeping track of smallest number.
	s = a[0];
	// A variable 'ss' keeping track of second smallest number.
	ss = a[0];
 
	// Traverse the data array.
	for(i = 1; i < n; i++)
	{
		// If array element is lesser than current 's' value then update.
		if(s > a[i])
		{
			ss = s;
			s =  a[i];
		}
		// Otherwise the number can be second smallest number so check for the condition and update 'ss'.
		else if(ss > a[i])
		{
			ss = a[i];
		}
	}
	// Return second smallest number.
	return ss;
}
 
int main()
{
	int n, i;
	cout<<"Enter the number of element in dataset: ";
	cin>>n;
 
	int a[n];
	// Take input.
	for(i = 0; i < n; i++)
	{
		cout<<"Enter "<<i+1<<"th element: ";
		cin>>a[i];
	}
 
	// Print the result.
	cout<<"\n\nThe second Smallest number of the given data array is: "<<SecondSmallest(a, n);
 
	return 0;
}
*/
//user inputs numbers.
//if the number is divisible by any number then it is not prime
/*
int main () {
//checking for prime numbers based on input.
    int n;
    while (cin >> n) { //while the user inputs numbers.
        for (int i = 2; i <= (n-2); i++) { //checks from 2 to n-2 if n divides without a remainder.
            if ((n%i == 0)) {
                cout << "0 \n";
                break;
            }else {
                cout << "1 \n";
                break;
            }
        }
    }
    return 0;
}
*/
/*
int main () {
//checking for prime numbers based on input.
    int n;
    bool prime_number = true;
    
    while (cin >> n) { //while the user inputs numbers.
        bool prime_number = true;
        if (n == 0 || n == 1) {
            cout << n << " is not a valid number. \n";
        }
        if (n == 3) {
            prime_number = true;
        }
        if (n!=3 || n >= 2 || n < 8) { //if number is less than 8, if it is divisible by 2 or 3
            if (n%2 == 0 || n%3 == 0) {
                prime_number = false;
            }
        }
        if (n >= 8) {
            if (n%2 == 0 || n%3 == 0 || n%5 == 0 || n%7 == 0) {
                prime_number = false;
            }
        }
        if (prime_number) {
            cout << "0 \n";
        }else {
        cout << "1 \n";
        }
    }
    
    return 0;
} 
*/
/*
int main () {
    int n;
    int i;
    bool primeNumber = true;
    
    cin >> n;
    
    for (i = 2; i <= (n-2); i++) {
        if ((n % i) == 0) { //if n is divisible by any number then  it is not a prime number
            primeNumber = false;
            break;
        }
    }
    if (primeNumber) {
        cout << "1 \n";
    }else {
        cout << "0 \n";
    }
    return 0;
}*/
/*
int main () {
    
    int n;
    int i;
    bool prime = true;
    
    while (cin >> n) {
        prime = true; //don't forget to restate the boolean after every loop so it won't be stuck at prime=false.
        if (n < 0) {
            cout << "Prime numbers are not negative.";
        }
        if (n == 0 || n == 1) {
            cout << n << " is not prime.";
        }
        for (i=2; i <= (n-1); i++) {
            if ((int)n%i == 0) {
                prime = false;
            }
        }
        if (prime == true) {
            cout << "1 \n";
        }else {
            cout << "0 \n";
        }
    }
    return 0;
}
*/
/*
2/15/18
FUNCTIONS
-Kind of like functions in calc
f: R -> R
  domain codomain
  
In C++, we don't have "real numbers" like in math. 
double f(double); //looks just like we are declaring a variable
codomain domain

Important differences:
1. c++ functions can have side effects ex: printin to stdout, change global variables.
Hence, same input might not give same output.

(outside of main)
int x=0; //global variable (outside of every function) //global variables can be confusing at times, as they can tie many things together.
int f(int y); {//this function will take an integer y and perform whatever is inside of the function. 
return (y * x++) //the output of this function will change every time, it is tied to the global variable which will increment by 1.
}
int main () {
    while (true)
    cout << f(1);
    return 0;
}
C++ functions must have a concrete description on how to compute the answer.
Must put everything in complete detail.
The function must be provided with complete instructions on how to perform what you want it to do.
If you have a function in main, it won't be seen by the function.

Quick note: Say A,B, are finite sets. How many functions are there from A to B. What is |{f: A -> B}|
Say A = {a1,a2,a3...an} 




*/
/*
int main () {
    
    int number;
    bool prime_number = true;
    //Make a loop that is endless so the user can keep inputting variables.
    while (cin >> number) {
        prime_number = true; //don't forget to restate the boolean after every loop so it won't be stuck at prime=false.
        if (number < 0) {
            cout << "Prime numbers are not negative.";
        } //1 and 0 are not prime numbers
        //if (number = 2) { //2 is prime but my algorithm is incompatible with 2 because it starts at 2 and goes UP to n-1 which is 1.
        //    cout << "1 \n";
        //}
        if (number == 0 || number == 1) {
            cout << number << " is not prime.";
        }
		//check if the number inputted is divisible by numbers leading up to it.
		//we can't check for 1 cause all numbers are divisible by 1, so use 2.
		// if the number is divisible by a preceeding number, it is not prime.
        for (int i=2; i <= (number-1); i++) {
            if ((int)number%i == 0) {
                prime_number = false; 
				//break;
            }
        }
        if (prime_number == true) { //if as indicated the number is prime, cout 1
            cout << "1 \n"; 
        }else {
            cout << "0 \n";
        }
    }
    return 0;
}
*/
/*
bool isValid(int num) {
  if(num <= 0) {
    return false;
  } else {
    return true;
  }
}

bool isEven(int num) {
  if (num % 2 == 0) {
    return true;
  } else {
    return false;
  }
}

void calculate(int num) {
  if (num == 1) {
    cout << endl << "Finished!" << endl;
    return;
  } else if(isEven(num)) {
    num = num / 2;
    cout << num << " ";
    calculate(num); //call the function again so it will run everything
  } else {
    num = (num * 3) + 1;
    cout << num << " ";
    calculate(num); //same as comment above.
  }
}

int main() {

  int input;
  cout << "Please enter an integer greater than zero." << endl;
  cin >> input;

  if(isValid(input)) {
    calculate(input);
  } else {
    cout << "Not a valid input." << endl;
    return 0;
  }

  return 0;
}*/
/*
bool even_number (int n) {
    if (n % 2 == 0) {
        return true;
    }else {
        return false;
    }
}

bool odd_number (int n) {
    if (n % 2 == 1) {
        return true;
    }else {
        return false;
    }
}

void calculate (int n) {
    if (n == 1) {
        cout << "Done." << endl;
    }
    else if (even_number(n) == true) {
        n /= 2;
        cout << n << " ";
        calculate(n);
    }
    else if (odd_number(n) == true) {
        n = ((n*3) +1);
        cout << n << " ";
        calculate(n);
    }
}

int main () {
    
    int number;
    cin >> number;
    
    calculate(number);
    
    return 0;
}
*/
/*
int main () {
    
    int number;
    bool prime_number = true;
    //Make a loop that is endless so the user can keep inputting variables.
    while (cin >> number) {
        prime_number = true; //don't forget to restate the boolean after every loop so it won't be stuck at prime=false.
        if (number < 0) {
            cout << "Prime numbers are not negative.";
        } //1 and 0 are not prime numbers
        //if (number = 2) { //2 is prime but my algorithm is incompatible with 2 because it starts at 2 and goes UP to n-1 which is 1.
        //    cout << "1 \n";
        //}
        if (number == 0 || number == 1) {
            cout << number << " is not prime.";
        }
		//check if the number inputted is divisible by numbers leading up to it.
		//we can't check for 1 cause all numbers are divisible by 1, so use 2.
		// if the number is divisible by a preceeding number, it is not prime.
        for (int i=2; i <= (number-1); i++) {
            if ((int)number%i == 0) {
                prime_number = false; 
				//break;
            }
        }
        if (prime_number == true) { //if as indicated the number is prime, cout 1
            cout << "1 \n"; 
        }else {
            cout << "0 \n";
        }
    }
    return 0;
}
*/
//2/16/18
/* A complex problem is often easier to solve by dividing it into several smaller parts,
each of which can be solved by itself.
These parts are sometimes made into functions in c++.

the function main() uses type int, and returns an integer (return 0;).
main() uses functions when called, and can solve the problem using them.

Advantages of functions:
A function is kept separate by limitations, unless they are included within the function itself.
Functions also separate the concept, from the implementation (how it is done).
Functions can help make the program easier to understand, and they can be reused.
Syntax:

<return type> <function name> (<parameter list>) {
    (function body)
    (some code goes here)
    (local statements)
}

when calling your function in main:
int main () {
    functionname(parameter you want to checl) //this will perform the actions of the function.
}

*/
/*
double areaCircle(int radius) {
    int area;
    int solution;
    int pi = 3.14;
    
    cout << "Enter the radius" << endl;
    cin >> radius;
    area = pi * (radius * radius);
    solution = area;
    
    return solution;
}

int main () {
    
    int n;
    int final_answer;
  
    cout << areaCircle(n) << endl;
    
    return 0;
} */

/* 2/22/18
Terminology:
Actual parameter - This is the variable or expression that is used as input for a function.

void exampleFunction (parameter 1, parameter 2 . . . parameter n) {
    
}

int z = 10
exampleFunction(z) will put z as one of the actual parameters.

Formal parameters - This is the placeholder variable used in the function definition.

int f(int x) {
    cout << x << endl;
    x *= 100
} Actual param = when calling the parameter in main
Formal parameter - When writing the function, its the variable that you used in the function (x in this case).

Question: What is the relationship of actual and formal parameters?

Formal is a copy of the actual for by-value calls.
Formal is a synonym of the actual parameters. It lives in its own memory elsewhere. This is true for reference calls.
the default for primers is by value (formal parameters are copied). copies are used by default. use by reference. using & will do this in the function parameters.
if the function doesnt have the &, it might have side effects.

void f(int x) {
    x += 10;
}void g (int&y) {
    y += 10;
}

int main () {

    int z = 70;
    f(z);
    g(z);
    
    return 0;
}

New topic: Vectors

#include <vector>

(Stores an indexed list of variables (with homogeneous type).
Example: read list of integers from stdin and print them in reverse order.
This would be hard to do, as we do not have a fixed number of variables.
Vectors let you store the whole list.
Solution (with vectors):

int main () {
   while (cin >> n) {
        v.push-back(n); //importing a library to use it
   }
}
say the input was 10 20 30...  v : 10 | 20 | 30
v.push_back(100); //you can only push back one number at a time
v would be: 10 | 20 | 30 | 100
Note:: Vectprs are templates. Think of it like a machine that accepts a datatype T, and produces a new datatype "vector of t's" 
int main () {

    vector<int> V; //supply the datatype in the <>.
    int n; //input
    while (cin >> n) {
        V.push-back(n); //how do we access the elements of V? Use square brackets and an index, first one is index 0.
        //if you want to know how many elements are in v, use v.size().
    }
    for (int i = V.size(), i >= 0, i++)
    
    
    return 0;
}
*/
/*
int main () {
    
    vector<int> v;
    int n;
    int counter = 0;
    
    do {
        cin >> n;
        v.push_back(n);
        counter++;
    }
    while (counter < 10);
    
    for (int i=v.size() - 1; i=0; i--) {
        cout << v[i] << endl;
    }
    
    return 0;
}
*/
/*
int main () {
    
    vector<int> v;
    
    for (int i = 1; i <= 10; i++) {
        v.push_back(i);
        cout << v[i] << endl;
    }
    
    return 0;
}
*/
/*
void index(vector<int> v, size_t start) { //gets the index of the smallest thing.
    
    start = 0;
    size_t i = start;
    for (size_t j = 0; j < v.size(); j++) {
        if (v[j] < v[i]) {
            i = j; //compares the vaLues of the now smallest to other potential < numbers in the vector.
        }
    }
    return i;
}

void sort() {
    for (size_t i = 0; i < v.size(); i++) {
        size_t smallest = index(v, start); //will return the smallest index
        int temp = v[smallest]; //setting an integer to the smallest value index.
        v[smallest] = v[i]; //initializing the smallest index to a value of i in the vector.
        v[i] = temp; //finally setting the value of v[i] to the value of temp.
    }
} */
/*
void characterSearch (string a, char c) {
    
    size_t b = a.length();
    int counter = 0;
    
    for (size_t i = 0; i < b; i++) {
        if (a[i] == c) {
            counter++;
        }
    }
    cout << counter << endl;
   
}

int main () {
    
    string test = "hello";
    char testing = 'l';
    
    characterSearch(test, testing);
    
    return 0;
} */

bool substring(string&shortString, string&longString, bool condition) { //checking if one string is contained inside of another
    
    size_t a = shortString.length();
    size_t b = longString.length();
    condition = false;
    
    for (size_t i = 0; i < b; i++) { //iterate through the long string
        for (size_t j = 0; j < a; j++) { //iterate through the short string
            if (shortString[j] = longString[i+j]) { //if the condition above is executed then it will begin a search on the longer string.
                condition = true;
            }
        } 
    }
    return condition;
}

int main () {
    
    string longerString = "band";
    string shorterString = "xyz";
    bool condition;
    
    substring(shorterString, longerString, condition);
    
    if (condition) {
        cout << "There is a match!" << endl;
    }else {
        cout << "No match" << endl;
    }
    
    return 0;
}


























