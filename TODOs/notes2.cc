#include <iostream>
using namespace std;
#include <vector>
#include <algorithm>
#include <string>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
//reverse order for a vector of size 5
/*int main() {
    
    vector<int> v;
    int n;
    //while the user enters values put them in a vector
    for (int i=1; i <= 5; i++) {
        cin >> n;
        v.push_back(n);
    }
    for (int i = v.size() - 1; i >= 0; i--) {
        cout << v[i] << endl;
    }
    
}*/
/*
int main () {
    
    vector<int> v;
    int n;
    int x;
    cout << "Please enter 5 integers!" << endl;
    for (int i = 1; i <= 5; i++) {
        cin >> n;
        v.push_back(n);
    }
    cout << "Enter a number to check." << endl;
    cin >> x;
    for (int i = 0; i <= v.size() - 1; i++) {
        if (x == v[i]) {
            cout << x << " is in the vector." << endl;
        }else {
            cout << x << " is not in the vector." << endl;
            break;
        }
    }
    
    return 0;
} */

/*
int main () {
    
    vector<int> v;
    int n;
    int left = 0;
    int mid;
    
    for (int i = 1; i <= 10; i++) {
        v.push_back(i);
    } int right = v.size() - 1;
    
    int check;
    cin >> check;
    
    while (left <= right) {
        mid = (left + right) / 2;
        if (check > v[mid]) {
            left = mid + 1 ; //think of a number line, the left would then be one more than the middle to check if the number is in between the right and the new left.
        }else if (check < v[mid]) {
            right = mid - 1;
        }else {
            cout << "Number found" << endl;
            break;
        }
    }
    return 0;
}
*/
/*
int main () {
    
    vector<int> v;
    int n;
    
    for (int i = 10; i >= 1; i--) {
        v.push_back(i);
    }
    
    for (int i = 1; i <= v.size(); i++) {
        for (int j = 0; j <= v.size() -1; j++) {
            if (v[i] < v[j]) {
                swap (v[i], v[j]);
            }
        }
    }
    
    for (int i = 0; i <= v.size(); i++) {
        cout << v[i] << endl;
    }
    
    
    return 0;
}


int main () {
    
    vector<int> v;
    int n;
    
    for (int i = 1; i <= 10; i++) {
        v.push_back(i);
    }
    for(int test=0; test <= v.size() -1; test++) {
        cout << v[test] << endl;
    }
    
    cout << "Enter a number" << endl;
    cin >> n;
    
    for (int j = 0; j < v.size() ; j++) {
        if (n == v[j]) {
            cout << "Number was found" << endl;
            break;
            
        }
    }
    return 0;
} */

/* 2/27/18
bool search(const vector<int>&v)

*/
/*
void findIndexOfSmallest (vector<int>&x, int n) { //call by reference because the values in main for this vector will differ.
                                                //call by value will potentially make a copy of a large vector.
    for (int i = 0; i <= n-1; i++) {
        for (int j = 1; j <= n; j++) {
            if (!(x[i] < x[j])) {
                swap (x[i], x[j]);
            }
        }
    }
    for (int k = n-1; k >= 0; k--) {
        cout << x[k] << endl;
    }
}

int main() {
    
    vector<int> v;
    int m;
    int elements;
    
    while (cin >> elements) {
        v.push_back(elements);
    }
    m = v.size();
    
    findIndexOfSmallest(v, m);
    
    return 0;
}

/*
void smallestVectorIndex (vector<int>&x) {
	
	//the vector will check x[0] and compare to other indexes. then the smallest value will become x[0]
	for (int i = 0; i < x.size(); i++) {
	    
		for (int j = 1; j <= x.size(); j++) {
		    
			if (x[i]>x[j]) {
			    
				swap (x[i],x[j]);
				
			}
		}
	}
	for (int k = 0; k < x.size(); k++) {
		cout << x[k] << endl;
	}
}

int main () {
	
	int n;
	vector<int> v;
	
	while (cin >> n) {
		v.push_back(n);
	}
	
	smallestVectorIndex(v);

	return 0;
}
*/


/*
void reverse(vector<int> v) {
    for (int i=v.size() -1; i = 0; i--) {
        for (int j = 0; j = v.size() -1; j++) {
            v[i] = v[j];
        }
    }
} */
/*
int main () {
    
    string test = "hello";
    
    string::iterator iter;
    for (iter = test.end(); iter >= test.begin(); iter--) {
        cout << test[iter];
    }
    
    return 0;
}
*/

//Project:
/* 
vector<vector<bool>> v; //produces a grid for the project
v[0] would make it a vector of booleans
v[0][1] would locate a specific value (horizontal and vertical)

v[i][j] would locate a point. to see adjacent values it would [i+1][j] etc.

reading files: on readme

vim test.sh
cd tests/

you have to send the output to 

./life -s res/glider-40x20
watch in the command line just executes a command repeatedly over a period of time.
cout << text[w[i][j]]

*/

//substrings:
/*
int main () {
    
    //To see if a string is a substring of another, we check for a match at all possible string points: 0 -> (s1.length() - s2.length())
    //how to check for a match string at i?
    
    //Match starts at i
    //s2[0] == s1[i] && s2[1] == s2[i+1] . . .
    
    bool subCheck;
    string s1 = "band"; 
    string s2 = "and";
    
    for (int i = 0; i < s1.length(); i++) {
        if (s2[0] == s1[i]) { //if the first character of the substring is = to i'th character of s1
            for (int j = 0; j < s2.length(); j++) {
                if (s2[j] == s1[i]) {
                    subCheck = true;
                }else {
                    subCheck = false;
                }
            }
        }
    }
    if (subCheck) {
        cout << "substring" << endl;
    }
    
    return 0;
} */
//when compiling, using -c is what you do when there is no main function.

/* 3/8/17 - Arrays, c-strings, and pointers
    Arrays are dumb versions of vectors.
    c-strings are dumb versions of strings.
    
    Arrays are just contiguous blocks of memory, along with convenient syntax.
    In main memory, each element is stored right next to each other.
    Arrays do not know their size, so unlike vectors we can't use .size().
    Different datatypes can be stored in one array, just use array[] when declaring it.
    
    What's really in an array?
    The memory address of the first element. It points to the first element.
    
    when you print A (just the array call with no parameter) it will cout the memory address that the array points to.
    
    How do you access elements? Just like a vector: use A[i], where i is any number.
    
    Pointers are variables that stores a memory address.
    Just like any other variable, but they store memory addresses.
    
    to create a pointer: int* p;
                         int x = 23;
                         p = &x; setting the pointer p to the address of x.
                         p DOES NOT equal 23, it points to where x is stored in memory.
                         x is basically a synonym for *p
                         23 now has 2 values associated to it: x and *p
                         
                         you can reassign *p = 99, and now both x and *p will = 99 instead of 23.
*/
/*
int subString (const string&a, const string&b, int i, int j) {
    //check to see if the first character of b is contained anywhere in a.
    for (i = 0; i < a.length() - b.length(); i++) { //if the string is long and substring is long, 
        
        for (j = 0; j < b.length(); j++) { //loop through shorter string

            if (!(b[j] == a[i+j])) {
                break;
            } 
        }
    }
    if (j = b.length()) {
        cout << i << endl;
    } 
    return 0; 
}

int main () {
    
    string s1 = "hello";
    string s2 = "llo";
    int i;
    int j;
    subString(s1, s2, i, j);
    
    return 0;
} */
/* Lab 3/9/18
Array: Basically just a contiguous block of variables with the same type.
Contiguous: Every element  is adjacent to the previous value in memory.

int A[5] //declaring an array with size 5
for (size_t i = 0; i < 5; i++) {
    A[i] = i * i (an array with squares up to 4).
}

Pointer: A pointer is a memory address of a variable.
It can be thought of as "pointing" to a specific variable's location in memory.
Every datatype has a corresponding pointer datatype.

Declaring them: int* p; //p is of type int*, int *p <- *p is of type int.
int x = 23;
p = &x; //the pointer now points to the address of x in memory.
x is now a synonym for *p, 23 has 2 variables associated with it now.

when incrementing with *p, do (*p)++

*/
/*
//Write a program that outputs the second smallest number.
int main () {
    
    int n; 
    vector<int> v;
    
    for (size_t i = 0; i < 5; i++) {
        cin >> n;
        v.push_back(n); */
    /*}
    for (int j = 0; j < v.size() -1 ; j++) {
        for (int k = 0; j < v.size(); k++) {
            if (v[j] > v[k]) {
                swap (v[j], v[k]);
            }
        }
        
    }*/ /*
    for (int i = 0; i <= v.size(); i++) {
        for (int j = 0; j <= v.size() -1; j++) {
            if (v[i] < v[j]) {
                swap (v[i], v[j]);
            }
        }
    }
    cout << v[2] << endl;
    return 0;
}
*/

/* 3/13/18

Polynomial evaluation:
-Store the coefficients in a vector so you can access them easily.


*/
/*
int polyEval(vector<int>c, int x) {
    
    int product = 0;
    
//    for (unsigned int i = 0; i <= c.size(); i++) { //Inefficient way to do this
//        product += (c[i] * pow(x,i)); //0 + ax^1 + a2x^2 + . . . + aix^i
//    }
    
    int xi = 1; //hold x^i
    for (int i = 0; i < a.size(); i++) {
        product += c[i] * xi;
        xi *= x; //when you multiply by x, it makes x^2 into x^3 etc etc.
    }
    
    return product;
}  

int main () {
    
    
    
    return 0;
} */
/*
int main () { //Circular shift
    
    int sizeofArray = 5;
    //Initialize an array of random integers.
    int theArray[sizeofArray];
    int temp[sizeofArray];
    
    for (int i = 0; i < sizeofArray; i++) {
        cin >> theArray[i];
    }
    
    for (int i = 0; i < sizeofArray; i++) {
        temp[i] = theArray[i];
    }
    
    //User enters a number to shift the array
    int n;
    cout << "Enter a number to shift the array by.";
    cin >> n;
    
    //First element will move n places to the right.
    //When we get to the (last element index - n) those will move to the first index up to (firstIndex + n).
    for (int j = 0; j < sizeofArray; j++) {
        
        if (j < (sizeofArray - n)) {
            theArray[j] = temp[j+n];
        }
            
        
    //When we go over the k'th element, it will replace beginning elements     
        else if (j >= (sizeofArray - n)) { //4
            int counter = 0;
    //Counter to go from 0 to size - n
            for (int k = (sizeofArray - n); k <= (sizeofArray - 1); k++) { //4 to 5
                    theArray[k] = temp[counter]; //temp is initialized as 1-5
                    counter++;
            }
                
        }
        
    }
    
    for (int b = 0; b < sizeofArray; b++) {
        cout << theArray[b] << endl;
    } 
    
    return 0;
} */
/*
int polyEval (int power, vector<int> coeff, int x) {
    
    int solution = 0;
    
    for (size_t i = power; i > 0; i--) {
        solution += (coeff[i-1] * pow(x, i));
    }
    for (size_t i = power; i > 0; i--) {
        cout << coeff[i-1] << " * " << pow(x, i) << " = " << (coeff[i-1] * pow(x, i) ) << endl;
    }
    return solution;
}

int hornersRule () {
    
    int solution = coeff[0];
    
    for (size_t i = 0; i < power; i++) {
        solution += (solution*x) + coeff[i]; //((4*x + 3)x + 2)x + 1)
    }
    
    return solution;
}

int main () {
    
    int degree;
    int coefficient;
    int xvalue;
    int constant;
    vector<int> storeCoefficient;
    
    //User enters the degree of the polynomial expression.
    cout << "Enter the degree of your expression" << endl;
    cin >> degree;
    
    //Stores the desired amount of coefficients into the vector for storage.

    cout << "Now enter the coefficients, note: they should equal the degree." << endl;
    
    for (size_t j = 0; j < degree; j++) {
        cin >> coefficient;
        storeCoefficient.push_back(coefficient);
    }
    
    cout << "Enter what you want the variable to equal" << endl;
    cin >> xvalue;
    
    if (!(degree == storeCoefficient.size())) {
        cout << "Degree must equal number of coeff." << endl;
    }
    
    int z = polyEval(degree, storeCoefficient, xvalue);
    
    cout << "Now enter a constant (if any)." << endl;    
    cin >> constant;
    cout << (z + constant) << endl;
    
    return 0;
} */

/* 3/20/18
Data structures: One variable that has a bunch of subvariables inside of it.
Vectors are an example of this.

Sets and maps:
Behind the scenes exist binary search trees. Refer to notes.
Subtree to the left contains values less than the original tree value, and the right is vice-versa.

For each comparison, you perform how many locations are ruled out. (Refer to diagram). (Looking for 49) 49 > 42, move to the
right of 42, 49 <77, move to left, etc, etc.

The approximate # of steps for search is # of times we can divide n by 2 while n/2 != 1.
n = total number of values

n = 2^k, k = logn / log2

Comparison without vectors:
PROS: Search = faster (= log n / log 2 steps) in csc log base 2 is the "natural" log.
In a vector, it searches in n steps, which takes more time.

Cons: No "random access" (In vectors we can do v[15] to instantly get the 15th element).
Non-retrievable by index.

Shared features:
Easy to add elements. Container grows automatically. v.push_back(x) == s.insert(x) <-- for sets.

Other differences:
Sets represent mathematical sets, and hence they cannot store duplicates.

Maps: JUst like sets, but you can attach extra data: 
42 | "Times sq"
The value 42 has Times sq attached to it (useful for databases).

M[42] = "Times sq".

//declaring sets is similar to vectors:
#include <set>
using std::set;

set<int> S;
//Filling S with perfect squares.

    for (int i = 1; i <= 100; i++) {
        S.insert( pow(i, 2) );
    }
    //Run some searches
    while (cin >> n) {
        if (S.find(n) != S.end()) { //end() method for sets just searches from beginning to end of the set.
            cout << n << " is a square" << endl;
            
        } 
    }
    //How do we print everything in a set? //Refer to notes on printing an array the strange way with pointers.
    for (set<int>::iterator i = S.begin(); i != S.end(); i++) { //Pointer to a location in the set, end would be the first thing that's off the set.
        cout << *i << endl; //since the iterator is a pointer, we would use *i to refer to the memory that i is pointing to.
    }
    
*/
/*
int main () {
    
    map<string,int> words;
    string n;
    
    for (int j = 0; j < 5; j++) {
        cin >> n;
        ++words[n];
    }
    
    typedef map<string,int>::iterator iter;
    
    for (iter i = words.begin(); i != words.end(); i++) {
        cout << i->first << ":" << i->second << endl;
    } //words is a map that maps strings to integers, it is of type pair<string,int> so first would correspond to the string, where second corresponds to integer value
    //or the number of occurences of each.
    
    return 0;
} */

/*
using .insert(make_pair("name",value) this shows that the elements of a map are pairs.
you can use maps to associate different maps with each other (refer to tb)

an example of iterating over a map:
if(dow_price.find("INTC")!=dow_price.end() ) //seeing if a value exists in a map. 

mapName["value"] = 81.23

correspondingVariable = mapName["value"];
*/
/* 3/22/18
Maps: Just like a set, but with extra data attached to each node.

map<string,int> f;

string s;
while (cin >> s) { 
    f[s]++; //increments the int that is associated with the string in the map.
}//if string s is not in the map then accessing f[s] creates it and the default int = 0.
//now just print f.

//when you put data into the map it generates a binary tree.

for (map<string,int>::iterator i = f.begin(); i != f.end(); i++) {
    cout << (*i).first << ":" << (*i).second << endl;
}

Ways to think about maps:
- like a vector, but indexes don't have to be integers (they do have to be orderable, that is that elements can be compared).
- gives a convenient way to partially define a function.
- 
*/

/* Recursion

Review with induction:
Want to prove a statement T that depends on int n. So T(n) = true for n.

Idea: Show T explicitly for some small value of n.
      Then in general, if T(n-1) is true then T(n) must be true.
      Conclude T(n) true for all values n.
      
      (Refer to notes)
      
      Other proofs apply too:
      If A then B
      
      A -> S1 (SAS)
      S1 -> S2 (algebra)
      S2 -> B
      
      Notice that there are a fixed number of implications. Induction 
      gives you a way to write proofs with an arbitrary number of implications.
      
      Say n0 = 1.
      T(1) => T(2) . . . => T(n)
      
      Think of induction as a "proof machine".
      
      Recursive functions specialize in mathematical induction.
      The idea is as follows:
      We want to write a program that computes some function F from some range of numbers a -> b.
      
      T(n) = our program sets the right answer on any input of size n.
      
      New programming technique (Recursion):
      - Hard-code answer for small inputs. Ex: An array of size 1 is already sorted.
      - Assuming the program works for smaller inputs, build the solution to your input of size n.
        This may involve calling your own function!
      - Celebrate?
      
      Easy example:
      Compute n!
      
      int factorial(int n) {
      
          if (n==0) {
              return 1;
          }
          
          return factorial(n-1) * n; //Meme formats
      } //Think of this ^^ as a copy+paste of the function until it can't perform anything anymore.
      
      */
      
/* 3/29/18

*From recursive 0 1 2 3 example
functions must return something first.

*/
/*
int fibrecurs(int n) {
    
    if (n < 2) {
        return 1;
    }
    
    return fibrecurs(n-1) + fibrecurs(n-2); //This will create 2 function calls.
    
}

int main () {
    
    cout << fibrecurs(5) << endl;
    
    return 0;
} */
/*
void f(int n) {
	if (n == 0) {
		cout << 0 << "\n";
		return;
	}
	f(n-1);
	cout << n << "\n"; //Think of recursive functions as like copy pasting the function
	/* TODO: make sure you can trace the sequence of recursive calls that
	 * would result from calling say, f(3).  Flip the order of the cout
	 * statement and the recursive call, and make sure you understand
	 * the output in both cases. */
 
/*
void selectionSort (vector<int>&v) {
    
    int min;
    
    for (size_t i = 0; i < v.size() -1; i++) {
        min = i;
        for (size_t j = i+1; j < v.size(); j++) {
            if (v[j] < v[min]) {
                //SET MINIMUM VALUE TO j INDEX of v
                min = j; 
                int temp = v[min]; //smallest thing we've seen, temporary value to store it.
                v[min] = v[i]; //Sets the smallest thing we've seen so far as the i'th index
                v[i] = temp;
            }
        }
    }
    
    for (size_t j = 0; j < v.size(); j++) {
        cout << v[j] << endl;
    }
    
    
}

int main () {
    
    vector<int> v = {20, 50, 40};
    
    selectionSort(v);
    
    return 0;
}
*/
/*
void write_vertical(int n) {
    if (n < 10) {
        cout << n << endl; // n is one digit
        
    } 
    else { // n is two or more digits long
        write_vertical(n/10);
        cout << (n%10) << endl;
    }
}

int main () {
    
    int n = 2348;
    
    write_vertical(n);
    
    return 0;
} */
/*
void bubbleSort(int a[], int n) {
    //Import the size of the array as an integer to count down from
    //IDEA: Use the size of the array as a backwards counter (variable n)
    if (n == 1) {
        return;
    }
    
    for (int j = 0; j < (n - 1); j++) {
        
        if (a[j] > a[j+1]) {
            int temp = a[j];
            a[j] = a[j+1];
            a[j+1] = temp;
        }
        
    }
    //Sort for the total number of elements, the first element will be different every time because everything is being swapped.
    bubbleSort(a, (n-1));    
    
}

int main () {
    
    int arr[5] = {20, 40, 30, 10, 50};
    
    int numElement = (sizeof(arr) ) / sizeof(arr[0]);
    
    bubbleSort(arr, numElement);
    
    for (int i = 0; i < numElement; i++) {
        cout << arr[i] << " ";
    }
    
    return 0;
}
*/
/*
void mergeSort() {
    
    for 
    
    
}

int main() {
    
    int numElement = (sizeof(arr)) / (sizeof(arr[0]));
    int mid = numElement / 2;
    int arr[5] = {20, 30, 10, 50, 40};
    
    return 0;
} */

/* 4/10/18

GCD: you have integers a,b, and r.
Find the biggest number that divides a b and r without a remainder.

Since a = qb+r, r = a-qb

gcd(a,b) = gcd(b,r)

r < b

Crucial to call recursive functions for smaller values than the one you already have

Recall the meta:
1. Solve small instances explicitly (base case).
2. Pretend your function works on all smaller inputs, and use that to build to your instance.

Question: What do we consider the "size" of a gcd instance (a,b)?
Base case: b = 0

int gcd (int a, int b) {
    //base case
    if (b == 0) {
        return a;
    }
    
    return gcd(b, (a%b));
    
}

Fastest sorting algorithm will take n * log n steps (n = total elements).

*/

/*
Euclidean Algorithm:

Have 2 numbers, replace the number on the left with the one on the right (assuming left > right) and the new right number is the remainder of left/right.

If the right number equals 0 after repeating this process then the left number is the GCD.

*/
/*
int gcd(int a, int b) {
    //Euclidean algorithm
    if (b == 0) {
        return a;
    }
    
    return gcd(b, a%b);
    
    return 0;
}

int main () {
    
    int number = gcd(20,6);
    
    cout << number << endl;
    
    return 0;
} */
/*
void leftSort(int a[], int counter) {
    
    int mid = (sizeof(a) ) / (sizeof(a[0]) ) / 2;
    
    if (counter == 0) {
        return;
    }
    
    for (int j = 0; j <= mid; j++) {
        
        if (a[j] > a[j+1]) {
            int temp = a[j];
            a[j] = a[j+1];
            a[j+1] = temp;
        }
        
    }
    
    leftSort(a, counter-1);
}

void rightSort(int a[], int counter) {
    
    int mid = (sizeof(a) ) / (sizeof(a[0]) ) / 2;
    
    if (counter == 0) {
        return;
    }
    
    for (int j = mid; j <= (mid * 2) + 2; j++) {
        
        if (a[j] > a[j+1]) {
            int temp = a[j];
            a[j] = a[j+1];
            a[j+1] = temp;
        }
        
    }
    
    rightSort(a, counter-1);
}


int main () {
    
    int arr[] = {60, 50, 40, 30, 20, 10};
    
    int numElement = (sizeof(arr) ) / (sizeof(arr[0]) );
    int midPoint = numElement/2;
    int backwardsCounter = midPoint;
    
    leftSort(arr, backwardsCounter);
    rightSort(arr, backwardsCounter);
    
    for (int i = 0; i < numElement; i++) {
        cout << arr[i] << endl;
    }
    
    return 0;
}*/
/*
void mergeSort (int *a, int n, int *aux) {
    
    int mid = n/2;
    if (n < 2) { //Array has 1 element, no reason to sort.
        return;
    }
    //Sorts left half a[0] to a[mid]
    mergeSort(a, mid - 1, aux); //Aux is useful in doing the merge sort.
    
    //Sorts a[mid] to a[n-1]
    mergeSort(a+mid, n-mid, aux); //Remember that an array is a pointer. Adding mid to a will give u a[mid]
    
    merge(a,mid,a+mid,n-mid,aux); //Merge function in todo 16 folder.
    
    //copy aux to a.
    for (int j = 0; j < n; j++) {
        a[i] = aux[i];
    }
    
} */

//Extended GCD Algorithm 
/* If d = gcd(a,b)
d = ax+by

How do we find x and y?

gcd(12,18) = 6.

12*(-1) + 18*(1) = 6
x = -1, y = 1 (Infinitely many values, x and y are not unique).

*/

int xgcd(int a, int b, int&x, int&y) {
    
    if (b == 0) {
        x = 1; //remember a * 1 = a.
        y = 0;
        return a;
    }
    
    int xx, yy;
    int d = xgcd(b, (a%b), xx, yy);
    //d = b*xx + (a%b)*yy
    //NOTE: a = qb+r. r = a-qb
    //So, d = b*xx + (a-qb)*yy
    
    //So it equals a * yy + b(xx-q*yy).
    //Given this ^^, x = yy, and y = xx-qyy
    //then return D.
    
    
} //GOAL: Find x and y such that d = ax + by

//4/17/18
/*
Computing the power set.
For a set S, define the power set of S as P(s) = {x|x <= s}. (the set of all subsets)

The magnitude of |P(s)| = 2^|s| if |s| < infinity.
Another way to describe x<=s is a function fx(y): |s| -> {0,1}.

Ex: P({1,2,3}) = { {] {1} {2} {3] {1,2} {2,3} {1,3} {1,2,3} }
8 possible combinations, because P has 3 numbers in the set (1,2,3) and 2^3 = 8.

Let's try to find a recursive algorithm.
Note: if T <= s then P(T) <= P(s).

Say T = {1,2} then P(T) = { {} {1} {2} {1,2} } For s, we are missing the set for 3, which would be:

{ {3} {1,3} {2,3] {1,2,3} } <- if you look at this, to get from P(t) to this, just add a 3 into every set.
*/

set<set<int> > subSet(set<int>& a) {
    
    //Base case: if S = {}, return {s}
    //P = set<set<int> >(set<int>);
    //Otherwise let x be in S (assuming S is not empty).
    //T = S (with x element removed so that T <= S).
    //Now let Pt = P(t) (we can get this thing for free using recursion)
    //Ptx = same as Pt, but with x added to each element.
    
    if (a.size() == 0) {
        return set<set<int> >(a);
    }
    
}

int main () {
    
    return 0;
}


      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      

    
    
    
    
    
    
    

























































