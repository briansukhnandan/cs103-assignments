#include <iostream>
#include <vector>
#include <algorithm>
#include <string>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
using namespace std;

int gcd(int a, int b) {
    //Euclidean's Algorithm (Recursive)
    if (b > a) {
        int temp = b;
        b = a;
        a = temp;
    }
    //Base case
    if (b == 0) {
        return a;
    }
    
    gcd(b, (a%b));
}

int main () {
    
    cout << gcd(12,18) << endl;
    
    return 0;
} 

//IDEA: d = ax + by
//Let's examine the original base case: b == 0, d = ax, and we return a when b ==0, so x = 1 and y = 0.
//During the recursion, d = b*xx + (a%b)*yy
//If you convert the division into multiplication again, a = (quotient * b) + the remainder -> (or a = qb-r for short)
//

/* TODO: write the *extended* GCD algorithm, which returns gcd(a,b), but
 * also sets x and y such that ax + by = gcd(a,b).  We did this in lecture,
 * but try to do it here from scratch (don't look at the notes unless you
 * have to). */
 
 /*
int xgcd(int a, int b, int *x, int *y) {
    
    if (b == 0) {
        *x = 1;
        *y = 0;
        return a;
    }
    
    int xx,yy;
    
    Some guidelines:
    r = a%b
    q = a/b 
    int d = xgcd(b, (a%b), &xx, &yy);
    
    *x = yy;
    *y = xx - (a/b) * yy;
    return d;
}

//void xgcdTest(int a, int b, int x, int y) {
    
//}

int main () {
    
    int x, y;
    cout << xgcd(12,6,&x,&y);
    
    
    
     
    
    return 0;
} */

/*
//ORGANIZATION:
//I used this format for the parameters: (number of disks, from-disk, to-disk, and then the middle disk)
void towerofHanoi(int n, const int a, const int c, const int b) {
    
    //Base case (If the largest disk is left on A, move it from A to C).
    if (n == 1) {
        cout << a << " " << c << " " << endl;
        return;
    }
    
    //STARTING POSITION: : | |  (pretend : is 3 dots)
    
    //Move n-1 disks from A to B using C.
    //In the Hanoi game, you rearrange the disks but eventually you get the middle peg to have n-1 disks.
    towerofHanoi(n-1, a, b, c);
    
    // . : | <-- This result was achieved by having . . ., then moving the right one to the middle one.
    //A hanoi representation: Move the smallest to the right, move 2nd smallest to the middle, you can move the smallest on top of the second smallest.
    
    cout << a << " " << c << endl;
    
    //Move n-1 disks from B to C using A.
    towerofHanoi(n-1, b, c, a);
    
}

int main () {
    
    towerofHanoi(3, 1, 3, 2);
    
    return 0;
}  */


/* 4/19/18

Permutations (bijective functions (1-1, onto) from a set to itself (finite).

Another way to think of it is all re-orderings of a list.

Ex: list = 1, 2, 3.
Give all the permutations of 1,2,3 (notice the order we put this in, it's a pattern).

1 2 3
2 1 3
1 3 2
3 1 2
2 3 1
3 2 1

For a list of size n, there are n! permutations.

Idea: For every element in the set, give it a turn to be last, and permute the first n-1 values in all possible ways.
*/
/*
//Input = vector, return a vector of vectors.
vector<vector<int> > Permutation(vector<int> L) {
    
    //Base case
    //Say n=1 and n= size of list, then return a list of lists
    //the lists in the bigger list should contain all the unique permutations.
    
    if (L.size() == 1) {
        vector<vector<int> > P;
        P.push_back(L);
        return P;
    }
    //IDEA: Permute all numbers except for the last one, then just stick the last number in. Do this for each element.
    n = L.size();
    for (int i = 0; i < L.size() - 1; i++) {
        //put L[i] last.
        swap(L[i], L[n-1]);
        
        int x = L[n-1];
        
        L.pop_back(); //Removes last element from the vector or list.
        
        vector<vector<int> > T = Permutation(L);
        //Insert x back into each vector.
        for (int j = 0; j <T.size(); j++) {
            T[j].push_back(x);
            P.push_back(T[j]);
        }
        L.push_back(x);
        swap(L[i], L[n-1]);
    }
    
    return P;
} */

/*
//Permutation TODO:
vector<vector<int> > perm(vector<int> v) {
    //Base case: If we have a vector of size 1, no need to permute it so return.
    vector<vector<int> > P;
    
    if (v.size() == 1) {
        //Store vector v inside of P.
        P.push_back(v);
        return P;
    }
    //At this point, P is a vector of vectors of all permutations we have seen so far.
    
    int n = v.size();
    
    //Loop in the vector v up to the element before the last element
    for (int j = 0; j < (n -1) - 1; j++) {
        //We'll put the majority of the program in this one for loop.
        //Swap the j'th element (it'll go up to the element before the last.)
        swap(v[j], v[n-1]);
        
        //Set a variable x to be the last element.
        int x = v[n-1];
        
        v.pop_back();
        
        //Will now loop and shift each variable, giving us the illusion of a permutation, and will continuously swap.
        vector<vector<int> > T = perm(v);
        //as indicated above.
        
        //Now to insert x back into everything
        for (int i = 0; i < T.size(); i++) {
            T[j].push_back(x); //Inserts x back into the elements of T, which contain the permutations of v.
            P.push_back(T[j]); //P is still the original vector that we are going to return, so put everything in T into x.
        }
        v.push_back(x); //Insert it back after T gets all the permutations and x is inserted into it.
        swap(v[j], v[n-1]);
    }
    
    return P;
}
*/ /*
int main()
{
	/* TODO: test this out more.  Here's some sample output from mine:
	 * $ echo {0..2} | ./perms
	 * 1 2 0
	 * 2 1 0
	 * 2 0 1
	 * 0 2 1
	 * 1 0 2
	 * 0 1 2
	 * 
	vector<int> V;
	int n;
	while (cin >> n) {
		V.push_back(n);
	}
	vector<vector<int> > R = perm(V);
	for (size_t i = 0; i < R.size(); i++) {
		for (size_t j = 0; j < R[i].size(); j++) {
			cout << R[i][j] << " ";
		}
		cout << "\n";
	}
	return 0;
} */

/*
4/24/18
Imagine you're a developer, how are function calls managed/implemented?

int f(int x) {
    int y;
    .
    .
    .
    return aa;
}

int main() {
    int z = 7;
    int w = f(z);
}

What needs to be stored?
-Return address (which instruction to do after the call).
-Storage for the parameters (like x in function f).
-Local variables (y in f(...) ).
-Storage for return value.

All this data is managed on a stack.
A stack is sort of like a vector where you can only access the last element.
A vector is a type of stack.
On x86 machines the stack grows downwards.
The return address stores the address of what to do next?

In function f, x, y, and aa are stored in memory.
In main, the return address, z, w, and the function call f(z) are stored.

When a new function call is implemented, a new stack frame is generated.
After the function call, the stack shrinks again.
Allocating space is the equivalent of subtracting things from the stack pointer and moving it down.

Homework: Read about "calling conventions".

Sometimes these behind the scenes details matter to the functionalty of your c++ program.

*/
/*
4/26/18
Dynamic memory
Allocate memory as the program is running.

int x; //allocated on the runtime stack.
//each reference to x
//x is referenced by an offset from the stack pointer.

NOTE: This offset is computed at compile time.

Any variable whose size is not known @ compile time could break this scheme.

In C++, dynamic allocations are done with the NEW operator E.g: 

int*p = new int;
*p = 99;

You can also allocate arrays:
int*A = new int[10];
Pointers are 8 bytes.
Not the array that is allocated to *A.

Remember: You ust deallocate whatever you allocated w/ new!
The consumption of memory will grow if you don't.
This is done VIA delet


*/
/*
4/27/18
A class is a user-defined type that specifies how objects of its type
can be classified or used.

class class_name {
    access_specifier_1
        member1;
    access_specifier_2
        member2;
}

private members are only accessible from within members of the same class.

public members are accessible anywhere from where the object is visible.

Ex:
class Rectangle {
    private:
        int width;
        int height;
    public:
        void set_value (int, int);
        int area () {
            return (width * height);
        }
}

void Rectangle::set_value(int a, int b) {
    width = a;
    height = b;
}

int main () {

    Rectangle rect;
    rect.set_value(5,4);
    
    cout << rect.area() << endl;
    
    return 0;
}

*/

/* Chapter 9 Notes:

class x {
    public:
        int m;
        int mf(int v) {
            int old = m;
            m = v;
            return old;
        }
    private:
};

X var; //var is a variable of type X.  DO NOT FORGET TO DO THIS.
var.m = 7;
var.mf(9);

A struct is a class where members are public by default. */

/* 5/1/18

A class is a way to define your own types (ex: complex numbers)
Ex: class to hold a square:

class square {
    private:
        double sideLength;
        double x; (x,y) will be the center of the square.
        double y;
    public:
        //You can define member functions in the class too.
        double area(); //These member functions are usually specified in a header file. These are just function calls.
        void translate(double x, double y);
        square();
        square(double ix, double iy, double iside); //CONSTRUCTOR
}; //Don't forget the semicolon at the end of a class.

int main () {
    square s;
    s.x = 0;
    s.y = 0;
    s.side = 10;
    
}

How to define member functions?
we want s.area() to return the area. How do we write it?

Non member function:
double area(square s) {
    return s.side * s.side;
}

Classes are basically used to define a namespace.
double square::area() {
    return (*this).side ** (*this).side;
    *this is a square, the pointer points to a square.
    
    Shorter version:
    return this->side * this->side;
    
    Even shorter (dangerous):
    return side * side;
}

Note: header file is enough to compile something using square but not enough to link it.

Constructors:
You can write special functions that will be called automatically upon a variable's creation.
In this case, if we gave the square class a constructor, this constructor would be called every time we declare a new sq.

Syntax:
Back to main:
square s; //Constructor called
square t(0,0,10); //Constructor called

Special constructors:
If you sre just setting member variables, do this:

square(double ix, double iy, double iside): x(ix), y(iy), side(iside) {};
//Equivalent to like x=ix, y=iy, side=iside (not efficient but works functionality wise).

5/3/18
What information is needed to represent a vector?
-Size
-Capacity
-Array for elements. (call this int* data)

Class invariant
- data points to an array of capacity elements.
- data[0], data[1], data[2] . . ., data[size-1] <- these store the contents of the vector.

we will make sure the above points will hold after any constructors are called. Then every member function
should assume the above after being called, and must ensure they are still true afterward.

Give the vector an initial capacity of initCapacity
Let's write a basic constructor:
vector::vector (size_t initCapacity) {
    this->size = 0;
    this->capacity = initCapacity;
    // make sure invariant holds 
    this->data = new int[this->capacity];
    //NOTE: Datatype of 'this' is vector* (pointer that points to a vector in memory).
}

void vector::push_back (int x) {
//Assumes the capacity is never 0.
    if (size == capacity) {
        //Expand the array.
        capacity *= 2;
        //Allocate data to a larger array/
        int* larger = new int[capacity];
        
        //Copy contents into the larger array.
        for (size_t i = 0; i < (this->data).size; i++) {
            larger[i] = this->data[i];
        }
        //delete the previous memory that had less space.
        delete[] this-> data;
        //Let the computer set this->data to an array with a larger size.
        this->data = larger;
    }

    //For a moment, pretend we have enough capacity.
    //Fill in the last element with the element we wanted pushed back.
    this->data[size] = x;
    size++; //You're using another element in the array so increase the size by 1.
}

Destructors:
Opposite of a constructor: called when a variable goes out of scope, or is explicitly freed via delete.
The main job of this is to free dynamically allocated memory.

vector::~vector() {
    delete [] data;
}

*/
/*
5/8/18
3 things are almost always needed when you are writing a class that manages dynamic memory:
1. copy constructor
2. assignment operators
3. destructor

What do these things do?
For example take 2 vectors U and V and make U=v.
First the size of u will equal v, then the capacity, then the contents.

The problem is when we switch u's data pointer to v, we didn't delete u's previous data, so the program will crash.
When u and v go out of scope, the destructor will be called, but since u and v point to the same thing then v's data will be attempted
to be deleted twice which will result in another crash.

Similar issues arises with the copy constructors.
They look like this:
vector2::vector2 (const vector2& v) return type name is the same as class.
This makes copies of other vector2's.

Copy constructors are also called to initialize parameters in a by-value function call. This is why
the parameters for copy constructors must be called by reference.

Another ex of why we need a copy constr.

Say we had a fn void f(vector2 U) {
    
}//destructor for u called here.

Solution: make "deep copies" . . . (not just the pointers but the entire array)

vector2::vector2(const vector2& V) {
    this->size = v.size;
    this->capacity = v.capacity;
    
    this->data = new int[this->capacity];
    for (size_t i = 0; i < this->size; i++) {
        this->data[i] = v.data[i];
    }
}


*/
/* 5/10/18 

assert() operator: if a boolean expression inside parethesis is true, nothing happens.
If it is false, it aborts/terminates the program.


#if 0 or #if NDEBUG

//if stuff in here is false, everything in here is treated as a comment.

#endif

List of topics: */

/* REVIEW

Recursion:

You need a base case

K-subsets exercise: (Possible on final)
Partition the solution space into those containing x and those not containing x. (x is some element of the set).

How do we compute the subsets not containing x?

Make a recursive call:
ksub(s\{x}, k);

How do we compute the subsets that do contain x?
ksub(s\{x}, k-1) and then add x to all of the resulting sets.

if (k >= (size of S) ) {
    return {};
}
if (k > s) {
    return {{}}; 
}

*/





/*

*/
/*
void primeNumbers(int left, int right) {
    
    bool prime;
    vector<int> primeList;
    set<int> primeList2;
    
    for (int j = left; j <= right; j++) {
        
        for (int k = 2; k < j/2; k++) {
            
            if ((j % k) == 0) {
                prime = false;
                break;
            }
            
            else {
                primeList.push_back(j);
            }
            
        }
        if (prime) {
            primeList.push_back(j);
        }
    }
    
  //  for (int i = 0; i < primeList.size(); i++) {
  //      cout << primeList[i] << endl;
  //  }
    
    
    for (int i = 0; i < primeList.size(); i++) {
        
        primeList2.insert(primeList[i]);
        
    }
    
    for (set<int>::iterator m = primeList2.begin(); m != primeList2.end(); m++) {
        cout << *m << endl;
    }
    
}

int main () {
    
    primeNumbers(5,20);
    
    return 0;
}
*/





















 