#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
using namespace std;
/*
int main() {
    
    vector<int> v;
    int n;
    int sum = 0;
    
    for (size_t i = 0; i < 5; i++) {
        cin >> n;
        v.push_back(n);
    }
    
    for (int j = 0; j < 5; j++) {
        sum += v[j];
    }
    
    cout << "Average: " << sum / 5 << endl;
    
} */
/*
int main () {
    
    int n;
    int sum = 1;
    
    for (int i = 0; i < 5; i++) {
        cin >> n;
        sum *= n;
    }
    cout << sum << endl;
    
    return 0;
} */
/*
int main () {
    
    int n;
    int expIndex;
    cin >> n;
    
    for (int i = 0; i < 4; i++) {
    //if n divided by an exponent value of 2 is divisible, and the next power after it is greater
    //than n /2, return that value of i.
        if (n % (int)(2 ^ (i)) == 0 ) {
            expIndex = i;
        }
    }
    cout << expIndex << endl;
    
    return 0;
} */
/*
//Finding highest exponent of two that a number is divisible by.
int main () {
    
    int n;
    int counter = 0;
    
    cin >> n;
    
    do {
        n /= 2;
        counter++;
    }while ((n%2) == 0);
    
    cout << counter << endl;
    
    return 0;
} */

//Collatz conjecture
/*
int main () {
    
    int initial_input;
    int n;
    int counter = 0;
    
    cin >> initial_input;
    
    n = initial_input;
    
    do {
        
        if ((n%2) == 0) {
            n = n/2;
        }
        else if ((n%2) == 1) {
            n = ((n*3) + 1);
        }
        
        counter++;
        
    }while (!(n == 1));
    
    cout << "For the number " << initial_input << endl;
    cout << "It terminated " << counter << " times." << endl;
    
    return 0;
} */
/*
void maximal(int n1, int n2, int n3) {
    
    if (n1 < n2) {
        n1 = n2;
    }
    if (n1 < n3) {
        n1 = n3;
    }
    
    cout << n1 << endl;
}

int main () {
    
    maximal(7,3,2);
    
    
    return 0;
}
*/
/*
bool isPrime(int n) {
    for (int i = 2; i <= ((n /2) + 1); i++) {
        if (n%i == 0) {
            cout << "Not Prime" << endl;
            return true;
            break;
        }else {
            cout << "Prime" << endl;
            return false;
        }
    }
}

int main () {
    
    isPrime(143);
    
    return 0;
}
*/
/*
int fibonacci(int n) {
    
    int term1 = 0;
    int term2 = 1;
    int next_term;
    
    for (int i = 3; i <= n; i++) {
        next_term = (term1 + term2); //term1 and 2 are set to 0 and 1, so the next term will add them.
        term1 = term2; //now term 1 will set to the value of term2.
        term2 = next_term; //and now term2 is the value of the previous term 1 and 2
    }
    cout << next_term << endl; 
}

int main () {
    
    fibonacci(5);
    
    return 0;
} */
/*
int main () {
    
    vector<int> v;
    int check;
    size_t left = 0;
    
    cin >> check;
    
    for (size_t i = 1; i <= 12; i++) {
        v.push_back(i);
    }

    size_t right = v.size() - 1;
    size_t midpoint = (left + right) / 2;
    
    while (left < right) {
        if (check < v[midpoint]) {
            right = midpoint - 1;
            midpoint = (left + right) / 2;
        }
        else if (check > v[midpoint]) {
            left = midpoint + 1;
            midpoint = (left + right) / 2;
        }
        else {
            cout << "Number found" << endl;
            break;
        }
    }
    
    return 0;
} */
/*
int main()
{
	int x = 9;
	while (x > 0) {
		if (x > 6 || x % 5) //with %, if the remainder evaluates to positive, it is true.
			cout << "x == " << x << "\n";
		x -= 2;
	}
	cout << "x == " << x << "\n";
	return 0;
} */
/*
int fn1(int& a, int b, int& c)
{
    a = b++;
    b += 10;
    c = ++a;
    return b;
}

int main()
{
    int x=3, y=5, z=13;
    cout << x << " " << y << " " << z << "\n";
    int r = fn1(x,y,z);
    cout << x << " " << y << " " << z << "\n";
	cout << r << "\n";
    return 0;
}
*/
/*
int main()
{
    int A[5] = {0,2,4,6,8};
    cout << A[1] << endl;
    cout << ++A[2] << endl;
    cout << *A << endl; //pointer a points to the first element in an array.
    int* p = &A[3];
    cout << *(p--) << endl; //will print the value of *p first, then increment downwards.
    cout << *p << endl;
    return 0;
} */
/*
int main(void)
{
	int x = 23;
	int y = 99;
	int* p = &x;
	int* q = &y;
	/* NOTE: while you can't predict the exact memory addresses, for some
	 * of the lines below, you should still have some idea of what it will
	 * look like (in particular, whether or not you will get the same
	 * address twice, or two different ones).
	 * 
	cout << "p  == " << p << "\n&x == " << &x << "\n";
	cout << "q  == " << q << "\n&y == " << &y << "\n";
	cout << "x  == " << x << "\n*p == " << *p << "\n";
	cout << "y  == " << y << "\n*q == " << *q << "\n";
	p = q;
	cout << "p  == " << p << "\n&x == " << &x << "\n";
	cout << "q  == " << q << "\n&y == " << &y << "\n";
	cout << "x  == " << x << "\n*p == " << *p << "\n";
	cout << "y  == " << y << "\n*q == " << *q << "\n";
	return 0;
} */
/*
void selectionSort(vector<int> v) {
    
    for (size_t i = 0; i < v.size(); i++) {
        int start = 0;
        for (size_t j = 1; j < v.size(); j++) {
            if (v[j] < v[start]) {
                start = j; //will set start to the j'th index, which is the smallest one.
            }
        }
        int temp = v[start]; //smallest number we've seen so far.
        v[start] = v[i]; //sets smallest number to v[i]
        //v[i] = temp;
    }
}

int main () {
    
    vector<int> v;
    
    for (int i = 12; i >= 1; i--) {
        v.push_back(i);
    }
    
    selectionSort(v);
    
    for (int i = 0; i < v.size() -1; i++) {
        cout << v[i] << endl;
    }
    
    return 0;
}
*/

int main () {
    
    
    
    return 0;
}